import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import state from './app.component';

@Injectable({
  providedIn: 'root',
})
export class LogicService {
  map: any;
  start: any;
  end: any;

  ogmap: any;

  mapsub: Subject<any>;

  states_list: any[] = [];

  constructor() {}

  async up(map: any) {
    const x: string[] = map.reduce((prev: any, next: any) => {
      return prev.concat(next);
    });

    let temp: any[] = [];

    for (let index = 0; index < this.start.length; index++) {
      let t = x.indexOf(index.toString());
      const i = t % map[0].length;
      const j = Math.floor(t / map[0].length);
      if (map[j - 1][i] != 'wall' && !this.containsOnlyNumbers(map[j - 1][i])) {
        temp.push({ v: [j, i], i: index.toString() });
      }
    }
    for (let index = 0; index < temp.length; index++) {
      map[temp[index].v[0]][temp[index].v[1]] = 'open';
      map[temp[index].v[0] - 1][temp[index].v[1]] = temp[index].i;
    }
  }
  async dawn(map: any) {
    const x: string[] = map.reduce((prev: any, next: any) => {
      return prev.concat(next);
    });
    let temp: any[] = [];
    for (let index = 0; index < this.start.length; index++) {
      let t = x.indexOf(index.toString());
      const i = t % map[0].length;
      const j = Math.floor(t / map[0].length);
      if (map[j + 1][i] != 'wall' && !this.containsOnlyNumbers(map[j + 1][i])) {
        temp.push({ v: [j, i], i: index.toString() });
      }
    }
    for (let index = 0; index < temp.length; index++) {
      map[temp[index].v[0]][temp[index].v[1]] = 'open';
      map[temp[index].v[0] + 1][temp[index].v[1]] = temp[index].i;
    }
  }

  async left(map: any) {
    let temp: any[] = [];
    const x: string[] = map.reduce((prev: any, next: any) => {
      return prev.concat(next);
    });

    for (let index = 0; index < this.start.length; index++) {
      let t = x.indexOf(index.toString());
      const i = t % map[0].length;
      const j = Math.floor(t / map[0].length);
      if (map[j][i - 1] != 'wall' && !this.containsOnlyNumbers(map[j][i - 1])) {
        temp.push({ v: [j, i], i: index.toString() });
      }
    }
    for (let index = 0; index < temp.length; index++) {
      map[temp[index].v[0]][temp[index].v[1]] = 'open';
      map[temp[index].v[0]][temp[index].v[1] - 1] = temp[index].i;
    }
  }
  async right(map: any) {
    let temp: any[] = [];
    const x: string[] = map.reduce((prev: any, next: any) => {
      return prev.concat(next);
    });
    for (let index = 0; index < this.start.length; index++) {
      let t = x.indexOf(index.toString());
      const i = t % map[0].length;
      const j = Math.floor(t / map[0].length);
      if (map[j][i + 1] != 'wall' && !this.containsOnlyNumbers(map[j][i + 1])) {
        temp.push({ v: [j, i], i: index.toString() });
      }
    }
    for (let index = 0; index < temp.length; index++) {
      map[temp[index].v[0]][temp[index].v[1]] = 'open';
      map[temp[index].v[0]][temp[index].v[1] + 1] = temp[index].i;
    }
  }

  finished(map: any) {
    return JSON.stringify(map) == JSON.stringify(this.ogmap);
  }

  containsOnlyNumbers(str: string) {
    return /^\d+$/.test(str);
  }
  checkmoves(map: any) {
    let res: any[] = [];
    const x: string[] = map.reduce((prev: any, next: any) => {
      return prev.concat(next);
    });
    for (let num = 0; num < this.start.length; num++) {
      let t = x.indexOf(num.toString());
      const i = t % map[0].length;
      const j = Math.floor(t / map[0].length);
      if (map[j - 1][i] != 'wall' && !this.containsOnlyNumbers(map[j - 1][i])) {
        res.push('up');
      }
      if (map[j + 1][i] != 'wall' && !this.containsOnlyNumbers(map[j + 1][i])) {
        res.push('dawn');
      }
      if (map[j][i + 1] != 'wall' && !this.containsOnlyNumbers(map[j][i + 1])) {
        res.push('right');
      }
      if (map[j][i - 1] != 'wall' && !this.containsOnlyNumbers(map[j][i - 1])) {
        res.push('left');
      }
    }

    return this.hardcopy(res);
  }

  async get_next_states(map: state) {
    let moves: any[] = [];
    const hard = new state(map.level, this.hardcopy(map.map), map.parent);
    var temp = this.checkmoves(this.hardcopy(hard.map));
    const hard_arr = JSON.stringify(hard.map);

    let sl: any[] = [];

    if (temp.includes('up') && !moves.includes('up')) moves.push('up');
    if (temp.includes('dawn') && !moves.includes('dawn')) moves.push('dawn');
    if (temp.includes('left') && !moves.includes('left')) moves.push('left');
    if (temp.includes('right') && !moves.includes('right')) moves.push('right');

    if (moves.includes('up')) {
      await this.up(map.map);
      sl.push(this.hardcopy(map.map));
      hard.map = JSON.parse(hard_arr);
      map.map = JSON.parse(hard_arr);
      map = hard;
    }
    if (moves.includes('dawn')) {
      await this.dawn(map.map);
      sl.push(this.hardcopy(map.map));
      hard.map = JSON.parse(hard_arr);
      map.map = JSON.parse(hard_arr);
      map = hard;
    }
    if (moves.includes('right')) {
      await this.right(map.map);
      sl.push(this.hardcopy(map.map));
      hard.map = JSON.parse(hard_arr);
      map.map = JSON.parse(hard_arr);
      map = hard;
    }
    if (moves.includes('left')) {
      await this.left(map.map);
      sl.push(this.hardcopy(map.map));
      hard.map = JSON.parse(hard_arr);
      map.map = JSON.parse(hard_arr);
      map = hard;
    }
    return this.hardcopy(sl);
  }

  hardcopy(json: any) {
    return JSON.parse(JSON.stringify(json));
  }

  har(posistion: any, ind: number) {
    console.log('here', this.end);
    console.log(
      this.end[ind][0] - posistion[0] + (this.end[ind][0] - posistion[0])
    );
    return this.end[ind][0] - posistion[0] + (this.end[ind][0] - posistion[0]);
  }

  
}
