import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { LogicService } from './logic.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  host: {
    '(document:keypress)': 'handleKeyboardEvent($event)',
  },
})
export class AppComponent {
  maps = new map().maps;
  loading = false;
  ogmap: any[];

  lvl: number = 1;

  map: any = this.maps[0].map;

  levels: number[] = [1, 2, 3, 4, 5, 6];
  start: any;
  end: any;

  maps_list: any[] = [];

  res: any[] = [];

  visited_maps: any[] = [];

  finish: boolean = false;
  val: state | any;

  first = true;

  minvisited: any = null;

  constructor(private logic: LogicService) {}

  set_lvl(lvl: number) {
    this.lvl = lvl;
    this.map = this.logic.hardcopy(this.maps[lvl - 1].map);
    this.ogmap = this.logic.hardcopy(this.map);

    this.start = this.maps[lvl - 1].conf?.map((s) => {
      return s.start;
    });
    this.end = this.maps[lvl - 1].conf?.map((s) => {
      return s.End;
    });
    for (let index = 0; index < this.start.length; index++) {
      this.map[this.start[index][0]][this.start[index][1]] = index.toString();
      this.ogmap[this.end[index][0]][this.end[index][1]] = index.toString();
    }
    this.logic.map = this.map;
    this.logic.start = this.start;
    this.logic.end = this.end;
    this.logic.ogmap = this.ogmap;
  }

  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key == 'w') this.logic.up(this.map);
    if (event.key == 's') this.logic.dawn(this.map);
    if (event.key == 'a') this.logic.left(this.map);
    if (event.key == 'd') this.logic.right(this.map);
  }

  // ---------------clicks---------------------------------
  check_bfs() {
    this.bfs(new state(0, this.map, null));
  }
  async check_dfs() {
    await this.dfs(await this.before_dfs());
  }
  check_ucs() {
    this.ucs(new state(0, this.map, null));
  }
  check_as() {
    this.as(new state(0, this.map, null));
  }

  // --------------------------------------------------

  visited(array: any) {
    for (let index = 0; index < this.res.length; index++) {
      if (JSON.stringify(this.res[index]) == JSON.stringify(array)) {
        return true;
      }
    }
    return false;
  }

  async before_dfs(): Promise<state> {
    let first = new state(0, this.logic.hardcopy(this.map), null);
    let temp: any[] = await this.logic.get_next_states(first);
    first.childrens = temp.map((el) => {
      return new state(1, el, first);
    });
    return first;
  }

  async bfs(map: state): Promise<any> {
    let res: any = {};
    this.visited_maps.push(JSON.stringify(this.logic.hardcopy(map.map)));
    let temp: any[] = await this.logic.get_next_states(
      new state(map.level, this.logic.hardcopy(map.map), map.parent)
    );
    temp = temp.filter((el) => {
      if (!this.visited_maps.includes(JSON.stringify(el))) {
        return el;
      }
    });
    temp = temp.map((el) => {
      return new state(map.level + 1, el, map);
    });
    map.childrens = [...temp];
    while (temp.length > 0) {
      if (this.logic.finished(temp[0].map)) {
        res = { res: temp[0], map: [] };
        break;
      } else {
        this.visited_maps.push(JSON.stringify(temp[0].map));
        let loc: any[] = await this.logic.get_next_states(temp[0]);

        loc = loc.filter((el) => {
          if (!this.visited_maps.includes(JSON.stringify(el))) {
            if (!this.containes_in(temp, JSON.stringify(el))) {
              return el;
            }
          }
        });
        loc = loc.map((el) => {
          return new state(temp[0].level + 1, el, temp[0]);
        });
        temp[0].childrens = [...loc];
        temp = [...temp, ...loc];
        temp.shift();
        loc = [];
      }
    }
    res.map.push(res.res.map);
    while (res.res.parent != null) {
      res.map.push(res.res.parent.map);
      res.res = res.res.parent;
    }
    console.log(res.map.reverse());
    console.log(this.visited_maps.length);
  }

  async ucs(map: state): Promise<any> {
    let res: any = {};
    this.visited_maps.push(JSON.stringify(this.logic.hardcopy(map.map)));
    let temp: any[] = await this.logic.get_next_states(
      new state(map.level, this.logic.hardcopy(map.map), map.parent)
    );
    temp = temp.filter((el) => {
      if (!this.visited_maps.includes(JSON.stringify(el))) {
        return el;
      }
    });
    temp = temp.map((el) => {
      return new state(map.level + 1, el, map);
    });
    map.childrens = [...temp];
    while (temp.length > 0) {
      if (this.logic.finished(temp[0].map)) {
        if (!this.minvisited) {
          const r = temp.shift();
          this.minvisited = r;
          temp = temp.sort((a, b) => a.level - b.level);
          temp = temp.filter((el) => {
            if (el.level < this.minvisited.level) return el;
          });
          res = { res: r, map: [] };
        } else {
          let r = temp.shift();
          let base = new state(r.level, r.map, r.parent);
          if (this.minvisited.level > base.level) {
            this.minvisited = base;
            res = { res: base, map: [] };
            temp = temp.sort((a, b) => a.level - b.level);
            const r = temp.map((el) => {
              return JSON.stringify(el.map);
            });
            const rs = new Set(r);
            let ars = Array.from(rs);
            ars = ars.filter((el) => {
              return !this.visited_maps.includes(el);
            });

            temp = temp.filter((el) => {
              if (ars.includes(JSON.stringify(el.map))) {
                ars.shift();
                return el;
              }
            });
            temp = temp.filter((el) => {
              if (el.cost < this.minvisited.cost) return el;
            });
          }
        }
      } else {
        this.visited_maps.push(JSON.stringify(temp[0].map));
        let loc: any[] = await this.logic.get_next_states(temp[0]);

        loc = loc.filter((el) => {
          if (!this.visited_maps.includes(JSON.stringify(el))) {
            return el;
          }
        });

        loc = loc.map((el) => {
          return new state(temp[0].level + 1, el, temp[0]);
        });
        temp[0].childrens = [...loc];
        temp = [...temp, ...loc];

        temp.shift();

        if (this.minvisited) {
          temp = temp.filter((el) => {
            if (el.level < this.minvisited.level) return el;
          });
        }
        temp = temp.sort((a, b) => a.level - b.level);
        temp = temp.filter((el) => {
          if (!this.visited_maps.includes(JSON.stringify(el.map))) {
            return el;
          }
        });

        loc = [];
      }
    }
    console.log(new state(res.res.level, res.res.map, res.res.parent));
    res.map.push(res.res.map);
    while (res.res.parent != null) {
      res.map.push(res.res.parent.map);
      res.res = res.res.parent;
    }
    console.log(res.map.reverse());
    console.log(this.visited_maps.length);
  }

  async as(map: state): Promise<any> {
    let res: any = {};
    this.visited_maps.push(JSON.stringify(this.logic.hardcopy(map.map)));
    let temp: any[] = await this.logic.get_next_states(
      new state(map.level, this.logic.hardcopy(map.map), map.parent)
    );
    temp = temp.filter((el) => {
      if (!this.visited_maps.includes(JSON.stringify(el))) {
        return el;
      }
    });
    temp = temp.map((el) => {
      return new state(map.level + 1, el, map);
    });
    temp.map((el) => {
      el.calc_cost(this.logic.hardcopy(this.end));
    });
    temp = temp.sort((a, b) => a.cost - b.cost);

    map.childrens = [...temp];
    while (temp.length > 0) {
      if (this.logic.finished(temp[0].map)) {
        const r = temp.shift();
        this.minvisited = r;
        temp = temp.filter((el) => {
          if (el.cost <= this.minvisited.cost) return el;
        });
        res = { res: r, map: [] };
      } else {
        this.visited_maps.push(JSON.stringify(temp[0].map));
        let loc: any[] = await this.logic.get_next_states(temp[0]);

        loc = loc.filter((el) => {
          if (!this.visited_maps.includes(JSON.stringify(el))) {
            return el;
          }
        });
        loc = loc.map((el) => {
          return new state(temp[0].level + 1, el, temp[0]);
        });
        loc.map((el) => {
          el.calc_cost(this.logic.hardcopy(this.end));
        });
        temp[0].childrens = [...loc];
        temp.shift();
        if (this.minvisited) {
          loc = loc.filter((el) => {
            if (el.cost < this.minvisited.cost) return el;
          });
        }
        temp = [...temp, ...loc];
        temp = temp.sort((a, b) => a.cost - b.cost);
        let rr: any[] = [];
        for (let index = 0; index < temp.length; index++) {
          if (temp[index].cost == temp[0].cost) {
            rr.push(temp[index]);
          } else break;
        }
        rr = rr.sort((a, b) => a.cost - a.level - (b.cost - b.level));
        temp = temp.slice(rr.length);
        temp = [...rr, ...temp];

        rr = [];
        loc = [];
      }
    }
    console.log(new state(res.res.level, res.res.map, res.res.parent));
    res.map.push(res.res.map);
    while (res.res.parent != null) {
      res.map.push(res.res.parent.map);
      res.res = res.res.parent;
    }
    console.log(res.map.reverse());
    console.log(this.visited_maps.length);
  }

  containsOnlyNumbers(str: string) {
    return /^\d+$/.test(str);
  }

  containes_in(temp: state[], val: any) {
    let t: boolean = false;
    for (let index = 0; index < temp.length; index++) {
      const el = temp[index].map;
      if (JSON.stringify(el) == val) {
        t = true;
        break;
      }
    }
    return t;
  }

  ///////////////////////////////////////////////////

  async dfs(map: state): Promise<any> {
    if (this.containes_in(map.childrens, JSON.stringify(this.ogmap))) {
      this.finish = true;
      const v = map.childrens.find((el) => {
        return this.logic.finished(el.map);
      });
      if (!this.val) {
        this.val = v;
      }
      if (v) {
        if (v?.level < this.val.level) {
          this.val = v;
        }
      }
      this.get_child(v);
      return;
    } else {
      for (let index = 0; index < map.childrens.length; index++) {
        if (this.finish) {
          break;
        } else {
          const ch = map.childrens[index];
          this.visited_maps.push(JSON.stringify(ch.map));
          let nc: any[] = await this.logic.get_next_states(ch);
          nc = nc.filter((el) => {
            if (!this.visited_maps.includes(JSON.stringify(el))) {
              return el;
            }
          });
          ch.childrens = nc.map((el) => {
            return new state(ch.level + 1, el, ch);
          });
          this.dfs(ch);
        }
      }
    }
  }

  //////////////////////////////////////////////////////
  get_child(val: any) {
    if (this.first) {
      this.first = false;
      let res: any[] = [];
      res.push(this.val.map);
      while (val.parent != null) {
        res.push(val.parent.map);
        val = val.parent;
      }
      console.log(res.reverse());
      console.log(this.visited_maps.length);
    }
  }
}

export default class state {
  level: number;
  cost: number;
  map: any[];
  parent: state | null;
  childrens: state[];
  constructor(lvl: number, map: any[], parent: state | null) {
    this.level = lvl;
    this.map = map;
    this.parent = parent;
    this.childrens = [];
    this.cost = lvl;
  }

  calc_cost(end: any) {
    let mer: any[] = this.map.reduce(function (prev: string, next: string) {
      return prev.concat(next);
    });
    for (let index = 0; index < end.length; index++) {
      let temp = mer.indexOf(index.toString());
      let j = Math.floor(temp / this.map[0].length);
      let i = temp - j * this.map[0].length;
      this.cost += Math.abs(j - end[index][0]) + Math.abs(i - end[index][1]);
    }
  }
}

export class map {
  maps = [
    {
      map: [
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'open', 'open', 'open', 'open', 'wall'],
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ],
      conf: [{ start: [1, 1], End: [1, 5] }],
    },
    {
      map: [
        ['wall', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'wall', 'open', 'wall'],
        ['wall', 'open', 'wall', 'open', 'wall'],
        ['wall', 'open', 'wall', 'open', 'wall'],
        ['wall', 'open', 'wall', 'open', 'wall'],
        ['wall', 'wall', 'wall', 'wall', 'wall'],
      ],
      conf: [
        { start: [1, 1], End: [4, 1] },
        { start: [1, 3], End: [4, 3] },
      ],
    },

    {
      map: [
        ['wall', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'open', 'open', 'wall'],
        ['wall', 'wall', 'open', 'wall', 'wall'],
        ['wall', 'open', 'open', 'open', 'wall'],
        ['wall', 'wall', 'wall', 'wall', 'wall'],
      ],
      conf: [
        { start: [3, 3], End: [1, 2] },
        { start: [1, 1], End: [3, 2] },
      ],
    },
    {
      map: [
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'wall', 'open', 'wall', 'open', 'wall'],
        ['wall', 'open', 'open', 'open', 'open', 'open', 'wall'],
        ['wall', 'wall', 'wall', 'open', 'wall', 'wall', 'wall'],
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ],
      conf: [
        { start: [1, 1], End: [2, 5] },
        { start: [1, 3], End: [2, 3] },
        { start: [1, 5], End: [2, 1] },
      ],
    },
    {
      map: [
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'open', 'wall', 'open', 'wall'],
        ['wall', 'open', 'open', 'open', 'open', 'wall'],
        ['wall', 'open', 'open', 'open', 'open', 'wall'],
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ],
      conf: [
        { start: [1, 1], End: [2, 1] },
        { start: [2, 2], End: [3, 2] },
        { start: [3, 3], End: [4, 3] },
        { start: [4, 4], End: [3, 4] },
      ],
    },
    {
      map: [
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
        ['wall', 'open', 'open', 'open', 'wall', 'wall'],
        ['wall', 'open', 'open', 'open', 'open', 'wall'],
        ['wall', 'open', 'wall', 'open', 'open', 'wall'],
        ['wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ],
      conf: [
        { start: [2, 2], End: [2, 3] },
        { start: [1, 3], End: [1, 3] },
        { start: [3, 1], End: [3, 3] },
        { start: [3, 3], End: [3, 4] },
        { start: [2, 4], End: [2, 4] },
      ],
    },
  ];
}
