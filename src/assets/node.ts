export class node {
  constructor(
    public pos: { i: number; j: number },
    public end: { i: number; j: number },
    public key: any,
    public state: any
  ) {}

  check_moves() {}
  get_next_state() {}
  move() {}
  equal() {}
  is_finished() {}
}
